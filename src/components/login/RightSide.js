import React from "react";
import { Image } from "react-bootstrap";
import "./RightSide.css";
import Logo from "./../../assets/logo3.png";

function RightSide() {
  return (
    <div>
      
      <Image className="image" src={Logo} />
    </div>
  );
}

export default RightSide;
