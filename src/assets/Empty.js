import React from "react";
import { Image } from "react-bootstrap";
import logo1 from "./logo1.png";
import "./Empty.css";

function Empty() {
  return (
    <div className="mt-5">
      <div className="d-flex justify-content-center">
        <Image src={logo1} className="image-empty" width={500} />
      </div>

      <p className="text">
        <h3>Vous n'avez pas encore rejoint aucun employé ..!</h3>
      </p>
    </div>
  );
}

export default Empty;
