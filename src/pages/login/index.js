import React from "react";
import { Col, Row } from "react-bootstrap";

import LeftSide from "../../components/login/LeftSide";
import RightSide from "../../components/login/RightSide";

import "./index.css";

function LoginPage() {
  return (
    <Row className="landing  ">
      <Col className="col-12 col-sm-6 mt-5">
        <LeftSide />
      </Col>

      <Col className="col-12 col-sm-6 ">
        <RightSide />
      </Col>
    </Row>
  );
}

export default LoginPage;
