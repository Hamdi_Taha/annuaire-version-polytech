import React, { useEffect, useState } from "react";
import "./LeftSide.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { FaEye, FaEyeSlash, FaKey } from "react-icons/fa";
import { FaAt } from "react-icons/fa";

import { useHistory } from "react-router-dom";

var validator = require("validator");

function LeftSide() {
  let history = useHistory();

  const [email, setEmail] = useState("");
  const [isDisabled, setIsDisabled] = useState();
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };
  /* useEffect(() => {
    if (validator.isEmail(email) && validator.isStrongPassword(password)) {
      setIsDisabled(true);
    } else {
      setIsDisabled(false);
    }
  }, [email, password]); */
  const requiredEmailMessage =
    "L'email doit être sous la forme 'example@example.com'";
  const requiredPasswordMessage =
    "Le mot de passe doit contenir au moins 8 caractères avec au moins une lettre majuscule, une lettre minuscule, un symbole spécial et un chiffre.";

  /*  const handleSubmit = (event) => {
    event.preventDefault();
    history.push("/annuaire");
  }; */
  const submit = (event) => {
    event.preventDefault();
    if (!validator.isEmail(email)) {
      setEmailError(requiredEmailMessage);
    } else if (!validator.isStrongPassword(password)) {
      setPasswordError(requiredPasswordMessage);
      setEmailError("");
    } else {
      history.push("/annuaire");
    }
  };

  const handleShowHide = () => {
    setShow(!show);
  };

  return (
    <div className="wrapper d-flex  ">
      <div className="login ">
        <h3 className="text-center">Login </h3>

        <form className="p-3 mt-3" onSubmit={submit}>
          <div className="input-group mb-2 mr-sm-2  ">
            <div className="input-group-prepend">
              <div className="input-group-text py-3">
                <FaAt />
              </div>
            </div>
            <input
              type="email"
              className="form-control "
              id="inlineFormInputGroupUsername2"
              placeholder="Email"
              onChange={handleEmailChange}
            />
            <p style={{ color: "red", fontSize: "11px" }}>{emailError} </p>
          </div>

          <div className="input-group mb-2 mr-sm-2">
            <div className="input-group-prepend">
              <div className="input-group-text py-3">
                <FaKey />
              </div>
            </div>

            <input
              type={show ? "text" : "password"}
              className="form-control  "
              id="inlineFormInputGroupUsername2"
              placeholder="Password"
              onChange={handlePasswordChange}
            />

            <div className="input-group-append">
              <span className="input-group-text py-3">
                {show ? (
                  <FaEye id="show_hide" onClick={handleShowHide} />
                ) : (
                  <FaEyeSlash id="show_hide" onClick={handleShowHide} />
                )}
              </span>
            </div>
            <p style={{ color: "red", fontSize: "11px" }}>{passwordError} </p>
          </div>
          <button
            className="btn mt-3 "
            type="submit"
            /* disabled={!isDisabled} */
            onClick={submit}
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
}

export default LeftSide;
