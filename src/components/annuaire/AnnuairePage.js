import { Button } from "react-bootstrap";
import store from "../../pages/annuaire/store";

import NavBar from "./NavBar";

import AnnTable from "./AnnTable";
import AddModal from "../modals/AddModal";

function AnnuairePage() {
  // Extract data from the Redux store state and assign to const

  const handleShow = () => {
    store.dispatch({ type: "SHOW" });
  };

  return (
    <div>
      <NavBar />

      <div className="d-flex justify-content-end mt-3 me-3">
        <Button onClick={handleShow} className="bg-primary">
          Ajouter
        </Button>
      </div>

      <AnnTable />

      <AddModal />
    </div>
  );
}

export default AnnuairePage;
