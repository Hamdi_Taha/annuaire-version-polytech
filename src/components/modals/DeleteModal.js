import { Button, Modal } from "react-bootstrap";
import store from "../../pages/annuaire/store";
import { toast } from "react-toastify";


function DeleteModal({ show, handleClose, id }) {
  const handleClickDelete = () => {
    store.dispatch({ type: "DELETE_EMPLOYEE", payload: { id } });

    toast.success('Supprimé avec succés  ...!', {
      position: "top-right",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      });

    handleClose();
  };

  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Header closeButton>
        <Modal.Title>Confirmation de la suppression</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Woohoo, Etes-vous sùr de vouloir supprimer cet employé ?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}  className=" bg-white text-primary">
          Annuler
        </Button>
        <Button variant="primary" onClick={() => handleClickDelete()}>
          Confirmer
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default DeleteModal;
