import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import store from "../../pages/annuaire/store";

function EditModal({ show, handleClose, id }) {
  const employees = useSelector((state) => state.employees);

  const [state, setState] = useState({
    nom: "",
    prenom: "",
    numero: "",
    email: "",
    poste: "",
  });

  useEffect(() => {
    const foundEmployee = employees.find((employee) => employee.id === id);

    setState({ ...foundEmployee });

    console.log(foundEmployee);
  }, [employees, id]);

  const handleEditSubmit = (e) => {
    e.preventDefault();

    store.dispatch({
      type: "UPDATE_EMPLOYEE",
      payload: { newEmployee: state },
    });
    toast.success("Modifié avec succés ...!", {
      position: "top-right",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    setState({ nom: "", prenom: "", numero: "", email: "", poste: "" });

    handleClose();
  };

  const handleChange = (e) => {
    //    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;

    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Header>
        <Modal.Title>Modifier un employé</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form className="row px-5">
          <Form.Control
            name="nom"
            className="p-2 mt-2  "
            type="text"
            required="required"
            placeholder="Nom "
            value={state.nom}
            onChange={(e) => handleChange(e)}
          />
          <Form.Control
            className="p-2 mt-2  row-9"
            name="prenom"
            type="text"
            required="required"
            placeholder="Prénom"
            value={state.prenom}
            onChange={(e) => handleChange(e)}
          />
          <Form.Control
            className="p-2 mt-2 row-9"
            type="number"
            name="numero"
            required="required"
            placeholder="Numéro"
            value={state.numero}
            onChange={(e) => handleChange(e)}
          />
          <Form.Control
            className="p-2 mt-2 row-9"
            type="email"
            name="email"
            required="required"
            placeholder="Email"
            value={state.email}
            onChange={(e) => handleChange(e)}
          />
          <Form.Control
            className="p-2 mt-2 row-9"
            type="text"
            name="poste"
            required="required"
            placeholder="Poste"
            value={state.poste}
            onChange={(e) => handleChange(e)}
          />

          <Modal.Footer>
            <Button
              onClick={handleClose}
              type="button"
              className=" bg-white text-primary"
            >
              Annuler
            </Button>
            <Button onClick={handleEditSubmit}> Enregistrer </Button>
          </Modal.Footer>
        </Form>
      </Modal.Body>
    </Modal>
  );
}

export default EditModal;
