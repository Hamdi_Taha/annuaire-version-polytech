import React from "react";

function NavBar() {
  return (
    <nav className="navbar navbar-default bg-secondary">
      <h3 className="font-weight-bold mt-2 p-2 text-white ">
        Gérer les employés
      </h3>
    </nav>
  );
}

export default NavBar;
