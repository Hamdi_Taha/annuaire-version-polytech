import { createStore } from "@reduxjs/toolkit";

const initialState = {
  employees: [],
  showModal: false,
};
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SHOW":
      return { ...state, showModal: true };

    case "HIDE":
      return { ...state, showModal: false };

    case "ADD_EMPLOYEE":
      const { employee } = action.payload;
      employee.id = state.employees.length + 1;
      return { ...state, employees: [...state.employees, employee] };

    case "DELETE_EMPLOYEE":
      const { id } = action.payload;
      let employees = state.employees.filter((emp) => emp.id !== id);
      return { ...state, employees: [...employees] };

    case "UPDATE_EMPLOYEE":
      const { newEmployee } = action.payload;

      const filtredEmployees = state.employees.filter(
        ({ id }) => id !== newEmployee.id
      );


      return { ...state, employees: [...filtredEmployees, newEmployee] };

      

    default:
      return state;
  }
};

const store = createStore(appReducer);

export default store;
