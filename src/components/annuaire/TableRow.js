import { Fragment, useState } from "react";
import { Button } from "react-bootstrap";
import { AiFillEdit, AiTwotoneDelete } from "react-icons/ai";

import DeleteModal from "../modals/DeleteModal";
import EditModal from "../modals/EditModal";

function TableRow({ item, index }) {
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);

  const [selectedItem, setSelectedItem] = useState(null);

  const toggleOpenDeleteModal = (id = null) => {
    setSelectedItem(id);

    setOpenDeleteModal((prevState) => !prevState);
  };

  const toggleOpenEditModal = (id = null) => {
    setSelectedItem(id);

    setOpenEditModal((prevState) => !prevState);
  };

  return (
    <Fragment>
      <tr>
        <td>{item.nom}</td>
        <td>{item.prenom}</td>
        <td>{item.numero}</td>
        <td>{item.email}</td>
        <td>{item.poste}</td>
        <td>
          <Button
            onClick={() => toggleOpenDeleteModal(item.id)}
            type="button"
            className="btn btn-danger btn-sm me-2"
          >
            <span>
              <AiTwotoneDelete />
            </span>
          </Button>

          <Button
            onClick={() => toggleOpenEditModal(item.id)}
            type="button"
            className="btn-warning pr-3 btn-sm"
          >
            <span>
              <AiFillEdit />
            </span>
          </Button>
        </td>
      </tr>

      {openDeleteModal && (
        <DeleteModal
          show={openDeleteModal}
          handleClose={toggleOpenDeleteModal}
          id={selectedItem}
        />
      )}

      {openEditModal && (
        <EditModal
          show={openEditModal}
          handleClose={toggleOpenEditModal}
          id={selectedItem}
        />
      )}
    </Fragment>
  );
}

export default TableRow;
