import "./App.css";

import LoginPage from "./pages/login";
//import HomePage from "./pages/home";

import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import showStore from "./pages/annuaire/store";
import { Provider } from "react-redux";
import AnnuairePage from "./components/annuaire/AnnuairePage";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <Router>
      <ToastContainer  />

      <Switch>
        <Route exact path="/">
          <LoginPage />
        </Route>

        <Route exact path="/login">
          <LoginPage />
        </Route>

        <Route exact path="/annuaire">
          
          <Provider store={showStore}>
            <AnnuairePage />
          </Provider>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
