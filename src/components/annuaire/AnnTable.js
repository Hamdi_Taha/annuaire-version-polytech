import { Form, Table } from "react-bootstrap";
import { useSelector } from "react-redux";
import Empty from "../../assets/Empty";

import TableRow from "./TableRow";

function AnnTable(state) {
  const employees = useSelector((state) => state.employees);

  if (employees.length === 0) {
    return <Empty />;
  }

  return (
    <div className="m-2 p-5">
      <Form>
        {employees.length > 0 && (
          <Table>
            <thead>
              <tr className="">
                <th>Nom</th>
                <th>Prénom</th>
                <th>Numéro</th>
                <th>Email</th>
                <th>Poste</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              {employees.map((item, index) => (
                <TableRow item={item} index={index} key={index} />
              ))}
            </tbody>
          </Table>
        )}
      </Form>
    </div>
  );
}

export default AnnTable;
